package com.backbase.assignment

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.backbase.assignment.ui.module.mainscreen.main.main.MainActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class ExampleInstrumentedTest {
    private val mockWebServer = MockWebServer()

    @Rule @JvmField
    public val mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)


    private var mockLayoutManager: LinearLayoutManager? = null
//    val playingNowMoviesAdapter: PlayingNowMoviesAdapter()

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.backbase.assignment", appContext.packageName)
    }

    @Before
    fun setup() {
        mockWebServer.start(8080)
        mockLayoutManager = Mockito.mock(LinearLayoutManager::class.java)
    }

    @Test
    fun defaultDisplay() {
       val recyclerView : RecyclerView  =mActivityRule.activity.findViewById(R.id.playing_now_rv)

        //assertThat(LayoutManager layoutManager) is provided to us by the assertj-android-recyclerview library.
        assertEquals(recyclerView.getLayoutManager(), mockLayoutManager);
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

}
