package com.backbase.assignment.ui.repo

import java.io.File
import java.io.InputStreamReader
import java.nio.Buffer

class MockResponseFileReader(path: String) {

    val content: String

    init {
        val reader = InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }
}

