package com.backbase.assignment.ui.module.mainscreen.main.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.backbase.assignment.ui.module.mainscreen.main.details.MovieSourceFactory
import com.backbase.assignment.ui.model.PlayingNowResponse
import com.backbase.assignment.ui.model.Results
import com.backbase.assignment.ui.repo.MovieRepo

class MainViewModel : ViewModel() {

    fun getPlayingNowList(): LiveData<PlayingNowResponse> {

        return MovieRepo().getPlayingNowResponse()
    }

    var userPagedList: LiveData<PagedList<Results>>
    private var liveDataSource: MutableLiveData<PageKeyedDataSource<Int,Results>>

    init {
        val itemDataSourceFactory = MovieSourceFactory()
        liveDataSource = itemDataSourceFactory.movieRepoLiveData
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(MovieRepo.PAGE_SIZE)
            .build()
        userPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
    }
}