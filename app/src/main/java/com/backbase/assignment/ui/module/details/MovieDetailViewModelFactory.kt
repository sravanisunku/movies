package com.backbase.assignment.ui.module.mainscreen.main.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.backbase.assignment.ui.repo.MovieRepo

class MovieDetailViewModelFactory(private val movieRepo: MovieRepo): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)){
            return MovieDetailsViewModel(movieRepo) as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}