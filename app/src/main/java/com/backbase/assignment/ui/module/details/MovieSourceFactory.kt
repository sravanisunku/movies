package com.backbase.assignment.ui.module.mainscreen.main.details

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.backbase.assignment.ui.model.Results
import com.backbase.assignment.ui.repo.MovieRepo

class MovieSourceFactory : DataSource.Factory<Int, Results>() {
    val movieRepoLiveData = MutableLiveData<PageKeyedDataSource<Int, Results>>()
    override fun create(): DataSource<Int, Results> {
        val movieRepo = MovieRepo()
        Log.v("MovieSourceFactory","MovieSourceFactory::::")
        movieRepoLiveData.postValue(movieRepo)
        return movieRepo
    }
}