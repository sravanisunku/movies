package com.backbase.assignment.ui.module.mainscreen.main.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.backbase.assignment.ui.model.MovieDetailResponse
import com.backbase.assignment.ui.repo.MovieRepo

class MovieDetailsViewModel(private val movieRepo: MovieRepo): ViewModel() {

    fun getMovieDetails(movieId:Int) : LiveData<MovieDetailResponse>{
       return movieRepo.getMovieDetailInfo(movieId)
    }
}