package com.backbase.assignment.ui.movie

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.databinding.PlayingNowChildLayoutBinding
import com.backbase.assignment.ui.model.PlayingNowResponse
import com.backbase.assignment.ui.model.Results
import com.squareup.picasso.Picasso

/**
 * Adapter for displaying Playing now list items
 */
class PlayingNowMoviesAdapter(
    val items: PlayingNowResponse,
    var onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<PlayingNowMoviesAdapter.ViewHolder>() {
    lateinit var playingNowChildLayoutBinding: PlayingNowChildLayoutBinding;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        playingNowChildLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()),
            R.layout.playing_now_child_layout,
            parent,
            false
        );
        return ViewHolder(playingNowChildLayoutBinding);
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items.results[position], onItemClickListener)

    override fun getItemCount() = items.results.size

    class ViewHolder(var playingNowChildLayoutBinding: PlayingNowChildLayoutBinding) :
        RecyclerView.ViewHolder(playingNowChildLayoutBinding.root) {

        fun bind(item: Results, onItemClickListener: OnItemClickListener) {
            Picasso.get().load(Uri.parse("https://image.tmdb.org/t/p/original${item.poster_path}"))
                .placeholder(
                    R.drawable.placeholder
                ).fit().centerCrop()
                .into(playingNowChildLayoutBinding.thumbnailIv);
            playingNowChildLayoutBinding.thumbnailIv.setOnClickListener(View.OnClickListener {
                onItemClickListener.onItemClick(
                    item
                )
            })
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Results?)
    }

}