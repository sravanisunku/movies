package com.backbase.assignment.ui.custom

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator

class RatingView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    View(context, attrs, defStyleAttr) {
    private var mViewWidth = 0
    private var mViewHeight = 0
    private val mStartAngle = -90f // Always start from top (default is: "3 o'clock on a watch.")
    private var mSweepAngle = 0f // How long to sweep from mStartAngle
    private val mMaxSweepAngle = 360f // Max degrees to sweep = full circle
    private var mStrokeWidth = 20f // Width of outline
    private val mAnimationDuration = 400 // Animation duration for progress change
    private val mMaxProgress = 100 // Max progress to use
    private var mDrawText = true // Set to true if progress text should be drawn
    private var mRoundedCorners =
        true // Set to true if rounded corners should be applied to outline ends
    private var mProgressColor: Int = Color.BLACK // Outline color
    private var mTextColor: Int = Color.BLACK // Progress text color
    private val mPaint // Allocate paint outside onDraw to avoid unnecessary object creation
            : Paint

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        initMeasurments()
        drawOutlineArc(canvas)
        if (mDrawText) {
            drawText(canvas)
        }
    }

    private fun initMeasurments() {
        mViewWidth = width
        mViewHeight = height
    }

    private fun drawOutlineArc(canvas: Canvas) {
        val diameter = Math.min(mViewWidth, mViewHeight)
        val pad = mStrokeWidth / 2.0.toFloat()
        val outerOval = RectF(pad, pad, diameter - pad, diameter - pad)
        mPaint.setColor(mProgressColor)
        mPaint.setStrokeWidth(mStrokeWidth)
        mPaint.setAntiAlias(true)
        mPaint.setStrokeCap(if (mRoundedCorners) Paint.Cap.ROUND else Paint.Cap.BUTT)
        mPaint.setStyle(Paint.Style.STROKE)
        canvas.drawArc(outerOval, mStartAngle, mSweepAngle, false, mPaint)
    }

    private fun drawText(canvas: Canvas) {
        mPaint.setTextSize(Math.min(mViewWidth, mViewHeight) / 5f)
        mPaint.setTextAlign(Paint.Align.CENTER)
        mPaint.setStrokeWidth(0f)
        mPaint.setColor(mTextColor)

        // Center text
        val xPos: Float = canvas.getWidth() / 2f
        val yPos = (canvas.getHeight() / 2 - (mPaint.descent() + mPaint.ascent()) / 2)
        canvas.drawText(
            calcProgressFromSweepAngle(mSweepAngle).toString() + "%",
            xPos,
            yPos,
            mPaint
        )
    }

    private fun calcSweepAngleFromProgress(progress: Int): Float {
        return mMaxSweepAngle / mMaxProgress * progress
    }

    private fun calcProgressFromSweepAngle(sweepAngle: Float): Int {
        return (sweepAngle * mMaxProgress / mMaxSweepAngle).toInt()
    }

    /**
     * Set progress of the circular progress bar.
     * @param progress progress between 0 and 100.
     */
    fun setProgress(progress: Int) {
        val animator = ValueAnimator.ofFloat(mSweepAngle, calcSweepAngleFromProgress(progress))
        animator.interpolator = DecelerateInterpolator()
        animator.duration = mAnimationDuration.toLong()
        animator.addUpdateListener { valueAnimator ->
            mSweepAngle = valueAnimator.animatedValue as Float
            invalidate()
        }
        animator.start()
    }

    fun setProgressColor(color: Int) {
        mProgressColor = color
        invalidate()
    }

    fun setProgressWidth(width: Float) {
        mStrokeWidth = width
        invalidate()
    }

    fun setTextColor(color: Int) {
        mTextColor = color
        invalidate()
    }

    fun showProgressText(show: Boolean) {
        mDrawText = show
        invalidate()
    }

    /**
     * Toggle this if you don't want rounded corners on progress bar.
     * Default is true.
     * @param roundedCorners true if you want rounded corners of false otherwise.
     */
    fun useRoundedCorners(roundedCorners: Boolean) {
        mRoundedCorners = roundedCorners
        invalidate()
    }

    init {
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    }
}
