package com.backbase.assignment.ui.module.mainscreen.main.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.backbase.assignment.R
import com.backbase.assignment.databinding.ActivityMainBinding
import com.backbase.assignment.ui.module.mainscreen.main.details.MovieDetailActivity
import com.backbase.assignment.ui.movie.PlayingNowMoviesAdapter
import com.backbase.assignment.ui.utils.EXTRA_MOVIE_ID;
import com.backbase.assignment.ui.model.Results
import com.backbase.assignment.ui.movie.PopularListMoviesAdapter


class MainActivity : AppCompatActivity(), PlayingNowMoviesAdapter.OnItemClickListener,
    PopularListMoviesAdapter.OnItemClickListener {
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var moviesAdapter: PlayingNowMoviesAdapter
    private lateinit var popularListMoviesAdapter: PopularListMoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.getSupportActionBar()?.hide();
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            .create(MainViewModel::class.java)

        activityMainBinding.playingNowRv.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        //Playing now Movies list adding to Recyclerview
        viewModel.getPlayingNowList().observe(this, Observer {
            moviesAdapter = PlayingNowMoviesAdapter(it, this)
            activityMainBinding.playingNowRv.adapter = moviesAdapter

        })
        popularListMoviesAdapter = PopularListMoviesAdapter(this)
        activityMainBinding.mostPopularRv.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //Popular Movies List adding to recyclerview
        viewModel.userPagedList.observe(this, Observer {
            popularListMoviesAdapter.submitList(it)
            activityMainBinding.mostPopularRv.adapter = popularListMoviesAdapter
        })
    }

    /**
     * Navigating to movie detail screen when click on movie item
     */
    override fun onItemClick(item: Results?) {
        var intent = Intent(this, MovieDetailActivity::class.java)
        Log.v("movie_id", (item?.id).toString())
        intent.putExtra(EXTRA_MOVIE_ID, item?.id);
        startActivity(intent)
    }

}
